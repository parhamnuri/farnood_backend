from django.contrib import admin
from .models import Certificate, CertificateRequest

@admin.register(CertificateRequest)
class CertificateRequestAdmin(admin.ModelAdmin):
    list_display = ['id', 'phone_number', 'email', 'name']

@admin.register(Certificate)
class CertificateAdmin(admin.ModelAdmin):
    list_display = ['request']