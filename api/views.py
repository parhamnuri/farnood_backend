from rest_framework.views import APIView
from rest_framework.response import Response
from .models import CertificateRequest, Certificate
from .serializers import CertificateRequestSerializer
from django.core.mail import send_mail
from django.http import HttpResponse

# keyPub = RSA.importKey(pubKey) # import the private key
# signer = PKCS1_v1_5.new(keyPub)
# digest = SHA512.new()
# digest.update(msg)
# signer.verify(digest, b64decode(sign))

class CertificateRequestVIew(APIView):
    def post(self, request):
        serializer = CertificateRequestSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            certificate_request = serializer.save()
            link = 'http://157.175.90.86:8000/verification/{}'.format(certificate_request.id)
            send_mail(
                "Verification",
                "Please verify your account using this link: {}".format(link),
                "me7909938@gmail.com",
                [request.data.get('email')],
                fail_silently=True,
            )
        
        return Response(serializer.data)

class CertificateVerification(APIView):
    def get(self, request, id):
        c = CertificateRequest.objects.get(pk=id)
        c.status = 'APPROVED'
        c.save()
        return HttpResponse('Your account has been activated')

class CertificatesInquiry(APIView):
    def post(self, request):
        queryset = CertificateRequest.objects.filter(phone_number=request.data.get('phone_number')).filter(status='APPROVED')
        queryset |= CertificateRequest.objects.filter(id=request.data.get('phone_number')).filter(status='APPROVED')
        data = []
        for q in queryset:
            data.append({
                'status': q.status,
                'id': q.id,
                'certificate_raw': q.certificate_raw,
                'certificate': q.certificates.all().first().certificate
            })
        return Response(data)