from django.db import models

from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA512, SHA384, SHA256, SHA, MD5
from Crypto import Random
from base64 import b64encode, b64decode

privKey = """-----BEGIN RSA PRIVATE KEY-----
MIICWwIBAAKBgGk/Hu5dixc3gtg0ZT2I076OvmWJbjort+PnEzxdFXCs2LEN1/1K
Xs4B7WW+oajURBSXSjbctjz5xx6dQhu1tcQsMRQwyn9MaZst9lwbz15ATryEaSMY
Jc5q5KVPsgj3XKMWFbLmlCJYGa2QvrfXtBsCTro04ZAY38mhh2DaHHq/AgMBAAEC
gYBWqHyFdEx0f2ajMvExa5o9yzcGT9SM3TZ61udFyrc+oXOCxY1Up4nMQlYAcqiV
ZMHx2mhnJ/B94ISNtpqaSlIw93LucrshXxhAgDvTP0WE2x7TBtaEL1h5Qe4/agB8
oaiiL/gy05G+Nnx6wldBPteQjfScdqL6FEPDQb1l1G1qcQJBALOV3oJXPKQgLiIA
ECsVSc9itY404W+rbWbgwtG0YEfooLLaDj1L8lrZgAptCb/Pc+qCxykzRHJJF/cO
y8resb0CQQCWB5CghgvkAc+u7d03wtY5Zhd2ly9FTf//UiVrobmj19guctKTV7h5
r0JgMhFFyJbv2JN+c8dVHVVPftZFwSArAkEAib6+eQMoxEral8GspR8jCisQTw9I
izrWIz7qr6bG4uEPwZ93pJSIRB7oExcnjzQ0GQBOes+UBIUEGMAEmIrPkQJAYE90
6YeDpJgBEaPC6iPaT7iZAVkmUFA1MVsziHema2aJca/mPA4I75yxeH1Ll0eglVGr
xXZflOUiuciLlvXWRwJAZX82sYU05wFKLbbCOOGNyfkxeRb5yzdn4S4A9p0cGUfx
2T/pDHgrhVzRhRAW8aPt0Mgldj3E4I745fOXurkAtA==
-----END RSA PRIVATE KEY-----"""

pubKey = """-----BEGIN PUBLIC KEY-----
MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgGk/Hu5dixc3gtg0ZT2I076OvmWJ
bjort+PnEzxdFXCs2LEN1/1KXs4B7WW+oajURBSXSjbctjz5xx6dQhu1tcQsMRQw
yn9MaZst9lwbz15ATryEaSMYJc5q5KVPsgj3XKMWFbLmlCJYGa2QvrfXtBsCTro0
4ZAY38mhh2DaHHq/AgMBAAE=
-----END PUBLIC KEY-----"""



class CertificateRequest(models.Model):
    name = models.CharField(max_length=64)
    phone_number = models.CharField(max_length=12)
    email = models.EmailField(blank=True, null=True)
    national_id = models.CharField(max_length=32)
    text = models.CharField(max_length=128, blank=True, null=True)
    public_key = models.TextField()

    STATUSES = (
        ('PENDING', 'Pending'),
        ('REJECTED', 'Rejected'),
        ('APPROVED', 'Approved'),
    )
    status = models.CharField(choices=STATUSES, default='PENDING', max_length=8)

    @property
    def certificate_raw(self):
        return '{},{},{},{},{},{}'.format(self.name, self.phone_number, self.email, self.national_id, self.text, self.public_key)

    def save(self, *args, **kwargs):
        result = super().save(*args, **kwargs)
        if self.status == 'APPROVED' and self.certificates.all().count() == 0:
            data = self.certificate_raw
            data = bytes(data, 'utf-8')
            keyPriv = RSA.importKey(privKey)
            signer = PKCS1_v1_5.new(keyPriv)
            digest = SHA512.new()
            digest.update(data)
            Certificate.objects.create(
                request=self, 
                certificate=str(b64encode(signer.sign(digest)))[2:-1]
            )
        return result

    def __str__(self):
        return self.phone_number + '#' +  str(self.pk)

class Certificate(models.Model):
    request = models.ForeignKey(CertificateRequest, on_delete=models.SET_NULL, blank=True, null=True, related_name='certificates')
    certificate = models.TextField()